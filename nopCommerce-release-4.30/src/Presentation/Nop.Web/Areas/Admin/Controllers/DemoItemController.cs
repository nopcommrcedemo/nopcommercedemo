﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Item;
using Nop.Services.Caching;
using Nop.Services.Customers;
using Nop.Services.ExportImport;
using Nop.Services.Item;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Item;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Web.Areas.Admin.Controllers
{
    public class DemoItemController : BaseAdminController
    {
        #region Fields

        private readonly IAclService _aclService;
        private readonly ICacheKeyService _cacheKeyService;
        private readonly IDemoItemModelFactory _itemModelFactory;
        private readonly IDemoItemService _itemService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ICustomerService _customerService;
        private readonly IExportManager _exportManager;
        private readonly IImportManager _importManager;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly IStaticCacheManager _staticCacheManager;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IStoreService _storeService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IWorkContext _workContext;

        #endregion
        public DemoItemController(IAclService aclService,
            ICacheKeyService cacheKeyService,
            IDemoItemModelFactory itemModelFactory,
            IDemoItemService itemService,
            ICustomerActivityService customerActivityService,
            ICustomerService customerService,
            IExportManager exportManager,
            IImportManager importManager,
            ILocalizationService localizationService,
            ILocalizedEntityService localizedEntityService,
            INotificationService notificationService,
            IPermissionService permissionService,
            IStaticCacheManager staticCacheManager,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IUrlRecordService urlRecordService,
            IWorkContext workContext)
        {
            _aclService = aclService;
            _cacheKeyService = cacheKeyService;
            _itemModelFactory = itemModelFactory;
            _itemService = itemService;
            _customerActivityService = customerActivityService;
            _customerService = customerService;
            _exportManager = exportManager;
            _importManager = importManager;
            _localizationService = localizationService;
            _localizedEntityService = localizedEntityService;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _staticCacheManager = staticCacheManager;
            _storeMappingService = storeMappingService;
            _storeService = storeService;
            _urlRecordService = urlRecordService;
            _workContext = workContext;
        }
        protected virtual void UpdateLocales(DemoItem item, DemoItemModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(item,
                    x => x.Name,
                    localized.Name,
                    localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(item,
                    x => x.Description,
                    localized.Description,
                    localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(item,
                    x => x.MetaKeywords,
                    localized.MetaKeywords,
                    localized.LanguageId);

                //_localizedEntityService.SaveLocalizedValue(item,
                //    x => x.MetaDescription,
                //    localized.MetaDescription,
                //    localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(item,
                    x => x.MetaTitle,
                    localized.MetaTitle,
                    localized.LanguageId);

                //search engine name
                var seName = _urlRecordService.ValidateSeName(item, localized.SeName, localized.Name, false);
                _urlRecordService.SaveSlug(item, seName, localized.LanguageId);
            }
        }
        protected virtual void SaveItemAcl(DemoItem item, DemoItemModel model)
        {
            item.SubjectToAcl = model.SelectedCustomerRoleIds.Any();
            _itemService.UpdateItem(item);

            var existingAclRecords = _aclService.GetAclRecords(item);
            var allCustomerRoles = _customerService.GetAllCustomerRoles(true);
            foreach (var customerRole in allCustomerRoles)
            {
                if (model.SelectedCustomerRoleIds.Contains(customerRole.Id))
                {
                    //new role
                    if (existingAclRecords.Count(acl => acl.CustomerRoleId == customerRole.Id) == 0)
                        _aclService.InsertAclRecord(item, customerRole.Id);
                }
                else
                {
                    //remove role
                    var aclRecordToDelete = existingAclRecords.FirstOrDefault(acl => acl.CustomerRoleId == customerRole.Id);
                    if (aclRecordToDelete != null)
                        _aclService.DeleteAclRecord(aclRecordToDelete);
                }
            }
        }
        protected virtual void SaveStoreMappings(DemoItem item, DemoItemModel model)
        {
            item.LimitedToStores = model.SelectedStoreIds.Any();
            _itemService.UpdateItem(item);

            var existingStoreMappings = _storeMappingService.GetStoreMappings(item);
            var allStores = _storeService.GetAllStores();
            foreach (var store in allStores)
            {
                if (model.SelectedStoreIds.Contains(store.Id))
                {
                    //new store
                    if (existingStoreMappings.Count(sm => sm.StoreId == store.Id) == 0)
                        _storeMappingService.InsertStoreMapping(item, store.Id);
                }
                else
                {
                    //remove store
                    var storeMappingToDelete = existingStoreMappings.FirstOrDefault(sm => sm.StoreId == store.Id);
                    if (storeMappingToDelete != null)
                        _storeMappingService.DeleteStoreMapping(storeMappingToDelete);
                }
            }
        }
        public IActionResult Index()
        {
            return RedirectToAction("List");
        }
        public IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCategories))
                return AccessDeniedView();

            //prepare model
            var model = _itemModelFactory.PrepareDemoItemSearchModel(new DemoItemSearchModel());

            return View(model);
        }
        [HttpPost]
        public virtual IActionResult List(DemoItemSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageDemoItems))
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = _itemModelFactory.PrepareDemoItemListModel(searchModel);

            return Json(model);
        }
        public virtual IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCategories))
                return AccessDeniedView();

            //prepare model
            var model = _itemModelFactory.PrepareDemoItemModel(new DemoItemModel(), null);

            return View(model);
        }
        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Create(DemoItemModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageDemoItems))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var item = model.ToEntity<DemoItem>();
                item.CreatedOnUtc = DateTime.UtcNow;
                item.UpdatedOnUtc = DateTime.UtcNow;
                _itemService.InsertItem(item);

                //search engine name
                model.SeName = _urlRecordService.ValidateSeName(item, model.SeName, item.Name, true);
                _urlRecordService.SaveSlug(item, model.SeName, 0);

                //locales
                UpdateLocales(item, model);

                _itemService.UpdateItem(item);

                //ACL (customer roles)
                SaveItemAcl(item, model);

                //stores
                SaveStoreMappings(item, model);

                //activity log
                _customerActivityService.InsertActivity("AddNewDemoItem",
                    string.Format(_localizationService.GetResource("ActivityLog.AddNewDemoItem"), item.Name), item);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Catalog.DemoItems.Added"));

                if (!continueEditing)
                    return RedirectToAction("List");

                return RedirectToAction("Edit", new { id = item.Id });
            }

            //prepare model
            model = _itemModelFactory.PrepareDemoItemModel(model, null, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }
    }
}