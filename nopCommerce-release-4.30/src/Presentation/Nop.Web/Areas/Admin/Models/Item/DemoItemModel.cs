﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Item
{
    public partial class DemoItemModel : BaseNopEntityModel, IAclSupportedModel, ILocalizedModel<DemoItemLocalizedModel>, IStoreMappingSupportedModel
    {
        #region Ctor

        public DemoItemModel()
        {
            if (PageSize < 1)
            {
                PageSize = 5;
            }

            Locales = new List<DemoItemLocalizedModel>();
            AvailableDemoItemTemplates = new List<SelectListItem>();

            SelectedCustomerRoleIds = new List<int>();
            AvailableCustomerRoles = new List<SelectListItem>();

            SelectedStoreIds = new List<int>();
            AvailableStores = new List<SelectListItem>();
        }
        #endregion
        #region Properties

        [NopResourceDisplayName("Admin.Item.Items.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Item.Items.Fields.Description")]
        public string Description { get; set; }

        [NopResourceDisplayName("Admin.Item.Items.Fields.CategoryTemplate")]
        public int DemoItemTemplateId { get; set; }
        public IList<SelectListItem> AvailableDemoItemTemplates { get; set; }

        [NopResourceDisplayName("Admin.Item.Items.Fields.MetaKeywords")]
        public string MetaKeywords { get; set; }

        [NopResourceDisplayName("Admin.Item.Items.Fields.MetaDescription")]
        public string MetaDescription { get; set; }

        [NopResourceDisplayName("Admin.Item.Items.Fields.MetaTitle")]
        public string MetaTitle { get; set; }

        [NopResourceDisplayName("Admin.Item.Items.Fields.SeName")]
        public string SeName { get; set; }

        [NopResourceDisplayName("Admin.Item.Items.Fields.PageSize")]
        public int PageSize { get; set; }

        [NopResourceDisplayName("Admin.Item.Items.Fields.AllowCustomersToSelectPageSize")]
        public bool AllowCustomersToSelectPageSize { get; set; }

        [NopResourceDisplayName("Admin.Item.Items.Fields.PageSizeOptions")]
        public string PageSizeOptions { get; set; }

        [NopResourceDisplayName("Admin.Item.Items.Fields.ShowOnHomepage")]
        public bool ShowOnHomepage { get; set; }

        [NopResourceDisplayName("Admin.Item.Items.Fields.IncludeInTopMenu")]
        public bool IncludeInTopMenu { get; set; }

        [NopResourceDisplayName("Admin.Item.Items.Fields.Published")]
        public bool Published { get; set; }

        [NopResourceDisplayName("Admin.Item.Items.Fields.Status")]
        public bool Status { get; set; }

        [NopResourceDisplayName("Admin.Item.Items.Fields.SortOrder")]
        public int SortOrder { get; set; }

        public IList<DemoItemLocalizedModel> Locales { get; set; }

        public string Breadcrumb { get; set; }

        //ACL (customer roles)
        [NopResourceDisplayName("Admin.Catalog.Categories.Fields.AclCustomerRoles")]
        public IList<int> SelectedCustomerRoleIds { get; set; }
        public IList<SelectListItem> AvailableCustomerRoles { get; set; }

        //store mapping
        [NopResourceDisplayName("Admin.Catalog.Categories.Fields.LimitedToStores")]
        public IList<int> SelectedStoreIds { get; set; }
        public IList<SelectListItem> AvailableStores { get; set; }

        public IList<SelectListItem> AvailableItems { get; set; }

        #endregion
    }
    public partial class DemoItemLocalizedModel : ILocalizedLocaleModel
    {

        [NopResourceDisplayName("Admin.Catalog.Items.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Items.Fields.Description")]
        public string Description { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Items.Fields.MetaKeywords")]
        public string MetaKeywords { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Items.Fields.MetaTitle")]
        public string MetaTitle { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Items.Fields.SeName")]
        public string SeName { get; set; }
        public int LanguageId { get; set; }
    }
}
