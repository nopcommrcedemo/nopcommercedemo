﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nop.Core.Domain.Item;
using Nop.Web.Areas.Admin.Models.Item;

namespace Nop.Web.Areas.Admin.Factories
{
    public interface IDemoItemModelFactory
    {
        /// <summary>
        /// Prepare category search model
        /// </summary>
        /// <param name="searchModel">Category search model</param>
        /// <returns>Category search model</returns>
        DemoItemSearchModel PrepareDemoItemSearchModel(DemoItemSearchModel searchModel);

        /// <summary>
        /// Prepare paged category list model
        /// </summary>
        /// <param name="searchModel">Category search model</param>
        /// <returns>Category list model</returns>
        DemoItemListModel PrepareDemoItemListModel(DemoItemSearchModel searchModel);

        /// <summary>
        /// Prepare category model
        /// </summary>
        /// <param name="model">Category model</param>
        /// <param name="category">Category</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>Category model</returns>
        DemoItemModel PrepareDemoItemModel(DemoItemModel model, DemoItem category, bool excludeProperties = false);
    }
}
