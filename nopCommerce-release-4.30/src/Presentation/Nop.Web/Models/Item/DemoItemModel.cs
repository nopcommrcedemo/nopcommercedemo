﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Item
{
    public partial class DemoItemModel : BaseNopEntityModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public string MetaTitle { get; set; }
        public string SeName { get; set; }
        public bool DisplayItemBreadcrumb { get; set; }
        public IList<DemoItemModel> ItemBreadcrumb { get; set; }
    }
}
