﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentMigrator.Builders.Create.Table;
using Nop.Core.Domain.Item;

namespace Nop.Data.Mapping.Builders.Item
{
    public partial class DemoItemBuilder : NopEntityBuilder<DemoItem>
    {
        public override void MapEntity(CreateTableExpressionBuilder table)
        {
            table
                .WithColumn(nameof(DemoItem.Name)).AsString(400).NotNullable()
                .WithColumn(nameof(DemoItem.MetaKeywords)).AsString(400).Nullable()
                .WithColumn(nameof(DemoItem.MetaTitle)).AsString(400).Nullable()
                .WithColumn(nameof(DemoItem.PriceRanges)).AsString(400).Nullable()
                .WithColumn(nameof(DemoItem.PageSizeOptions)).AsString(200).Nullable();
        }
    }
}
