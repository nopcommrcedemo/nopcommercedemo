﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentMigrator;
using Nop.Core.Domain.Item;

namespace Nop.Data.Migrations.Indexes
{
    public class AddItemSubjectToAclIX : AutoReversingMigration
    {
        public override void Up()
        {
            Create.Index("IX_Item_SubjectToAcl").OnTable(nameof(DemoItem))
                .OnColumn(nameof(DemoItem.SubjectToAcl)).Ascending()
                .WithOptions().NonClustered();
        }
    }
}
