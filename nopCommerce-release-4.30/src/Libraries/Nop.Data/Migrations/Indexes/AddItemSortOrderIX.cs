﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentMigrator;
using Nop.Core.Domain.Item;

namespace Nop.Data.Migrations.Indexes
{
    public class AddItemSortOrderIX : AutoReversingMigration
    {
        #region Methods

        public override void Up()
        {
            Create.Index("IX_Item_LimitedToStores").OnTable(nameof(DemoItem))
                .OnColumn(nameof(DemoItem.LimitedToStores)).Ascending()
                .WithOptions().NonClustered();
        }

        #endregion
    }
}
