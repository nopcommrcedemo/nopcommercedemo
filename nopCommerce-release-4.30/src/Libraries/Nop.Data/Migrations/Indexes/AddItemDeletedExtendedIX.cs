﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentMigrator;
using FluentMigrator.SqlServer;
using Nop.Core.Domain.Item;

namespace Nop.Data.Migrations.Indexes
{
    public class AddItemDeletedExtendedIX : AutoReversingMigration
    {
        #region Methods         

        public override void Up()
        {
            Create.Index("IX_Item_Deleted_Extended").OnTable(nameof(DemoItem))
                .OnColumn(nameof(DemoItem.Status)).Ascending()
                .WithOptions().NonClustered()
                .Include(nameof(DemoItem.Id))
                .Include(nameof(DemoItem.Name))
                .Include(nameof(DemoItem.SubjectToAcl))
                .Include(nameof(DemoItem.LimitedToStores))
                .Include(nameof(DemoItem.Published));
        }

        #endregion
    }
}
