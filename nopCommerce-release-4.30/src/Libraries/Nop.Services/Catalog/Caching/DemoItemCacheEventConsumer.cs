﻿using System;
using System.Collections.Generic;
using System.Text;
using Nop.Core.Domain.Item;
using Nop.Services.Caching;

namespace Nop.Services.Catalog.Caching
{
    public partial class DemoItemCacheEventConsumer : CacheEventConsumer<DemoItem>
    {/// <summary>
     /// Clear cache data
     /// </summary>
     /// <param name="entity">Entity</param>
        protected override void ClearCache(DemoItem entity)
        {
            RemoveByPrefix(NopCatalogDefaults.ItemsAllPrefixCacheKey);
            RemoveByPrefix(NopCatalogDefaults.ItemBreadcrumbPrefixCacheKey);
        }
    }
}
