﻿using System;
using System.Collections.Generic;
using System.Text;
using Nop.Core;
using Nop.Core.Domain.Item;

namespace Nop.Services.Item
{
    public interface IDemoItemService
    {
        void DeleteItem(DemoItem item);
        IList<DemoItem> GetAllItems(int storeId = 0, bool showHidden = false);
        IPagedList<DemoItem> GetAllItems(string itemName, int storeId = 0,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false, bool? overridePublished = null);
        string GetFormattedBreadCrumb(DemoItem item, IList<DemoItem> allItems = null,
            string separator = ">>", int languageId = 0);
        IList<DemoItem> GetAllItemsDisplayedOnHomepage(bool showHidden = false);
        DemoItem GetItemById(int itemId);
        void InsertItem(DemoItem item);
        void UpdateItem(DemoItem item);
        void DeleteItems(IList<DemoItem> items);
        List<DemoItem> GetItemByIds(int[] itemIds);
        IList<DemoItem> GetItemBreadCrumb(DemoItem item, IList<DemoItem> allitems = null, bool showHidden = false);
        //IList<DemoItem> SortItemsForTree(IList<DemoItem> source, int parentId = 0,
        //    bool ignoreCategoriesWithoutExistingParent = false);
    }
}
