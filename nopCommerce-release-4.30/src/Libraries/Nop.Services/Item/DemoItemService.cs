﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Item;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Stores;
using Nop.Data;
using Nop.Services.Caching;
using Nop.Services.Caching.Extensions;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Stores;

namespace Nop.Services.Item
{
    public partial class DemoItemService : IDemoItemService
    {
        private readonly CatalogSettings _catalogSettings;
        private readonly IAclService _aclService;
        private readonly ICacheKeyService _cacheKeyService;
        private readonly ICustomerService _customerService;
        private readonly IEventPublisher _eventPublisher;
        private readonly ILocalizationService _localizationService;
        private readonly IRepository<AclRecord> _aclRepository;
        private readonly IRepository<DemoItem> _itemRepository;
        private readonly IRepository<StoreMapping> _storeMappingRepository;
        private readonly IStaticCacheManager _staticCacheManager;
        private readonly IStoreContext _storeContext;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IWorkContext _workContext;
        public DemoItemService(CatalogSettings catalogSettings,
            IAclService aclService,
            ICacheKeyService cacheKeyService,
            ICustomerService customerService,
            IEventPublisher eventPublisher,
            ILocalizationService localizationService,
            IRepository<AclRecord> aclRepository,
            IRepository<DemoItem> itemRepository,
            IRepository<StoreMapping> storeMappingRepository,
            IStaticCacheManager staticCacheManager,
            IStoreContext storeContext,
            IStoreMappingService storeMappingService,
            IWorkContext workContext)
        {
            _catalogSettings = catalogSettings;
            _aclService = aclService;
            _cacheKeyService = cacheKeyService;
            _customerService = customerService;
            _eventPublisher = eventPublisher;
            _localizationService = localizationService;
            _aclRepository = aclRepository;
            _itemRepository = itemRepository;
            _storeMappingRepository = storeMappingRepository;
            _staticCacheManager = staticCacheManager;
            _storeContext = storeContext;
            _storeMappingService = storeMappingService;
            _workContext = workContext;
        }
        public virtual void DeleteItem(DemoItem item)
        {
            if(item==null)
                throw new ArgumentNullException(nameof(DemoItem));
            item.Status = 0;
        }

        public virtual void DeleteItems(IList<DemoItem> items)
        {
            if (items == null)
                throw new ArgumentNullException(nameof(items));

            foreach (var item in items)
            {
                DeleteItem(item);
            }
        }

        public virtual IList<DemoItem> GetAllItems(int storeId = 0, bool showHidden = false)
        {
            var key = _cacheKeyService.PrepareKeyForDefaultCache(NopCatalogDefaults.ItemsAllCacheKey,
                storeId,
                _customerService.GetCustomerRoleIds(_workContext.CurrentCustomer),
                showHidden);

            var items = _staticCacheManager.Get(key, () => GetAllItems(string.Empty, storeId, showHidden: showHidden).ToList());

            return items;
        }

        public virtual IPagedList<DemoItem> GetAllItems(string itemName, int storeId = 0, int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false, bool? overridePublished = null)
        {
            var query = _itemRepository.Table;
            if (!showHidden)
                query = query.Where(i => i.Published);
            if (!string.IsNullOrWhiteSpace(itemName))
                query = query.Where(i => i.Name.Contains(itemName));
            query = query.Where(i => i.Status==0);
            query = query.OrderBy(i => i.SortOrder).ThenBy(i => i.Id);
            if (overridePublished.HasValue)
                query = query.Where(c => c.Published == overridePublished.Value);

            if ((storeId > 0 && !_catalogSettings.IgnoreStoreLimitations) || (!showHidden && !_catalogSettings.IgnoreAcl))
            {
                if (!showHidden && !_catalogSettings.IgnoreAcl)
                {
                    //ACL (access control list)
                    var allowedCustomerRolesIds = _customerService.GetCustomerRoleIds(_workContext.CurrentCustomer);
                    query = from c in query
                            join acl in _aclRepository.Table
                                on new { c1 = c.Id, c2 = nameof(DemoItem) } equals new { c1 = acl.EntityId, c2 = acl.EntityName } into c_acl
                            from acl in c_acl.DefaultIfEmpty()
                            where !c.SubjectToAcl || allowedCustomerRolesIds.Contains(acl.CustomerRoleId)
                            select c;
                }

                if (storeId > 0 && !_catalogSettings.IgnoreStoreLimitations)
                {
                    //Store mapping
                    query = from i in query
                            join sm in _storeMappingRepository.Table
                                on new { c1 = i.Id, c2 = nameof(DemoItem) } equals new { c1 = sm.EntityId, c2 = sm.EntityName } into c_sm
                            from sm in c_sm.DefaultIfEmpty()
                            where !i.LimitedToStores || storeId == sm.StoreId
                            select i;
                }

                query = query.Distinct().OrderBy(i => i.SortOrder).ThenBy(i=> i.Id);
            }

            var unsortedItems = query.ToList();

            //paging
            return new PagedList<DemoItem>(unsortedItems, pageIndex, pageSize);
        }

        public virtual IList<DemoItem> GetAllItemsDisplayedOnHomepage(bool showHidden = false)
        {
            var query = from i in _itemRepository.Table
                        orderby i.SortOrder, i.Id
                        where i.Published &&
                        i.Status!=0 &&
                        i.ShowOnHomepage
                        select i;

            var items = query.ToCachedList(_cacheKeyService.PrepareKeyForDefaultCache(NopCatalogDefaults.ItemsesAllDisplayedOnHomepageCacheKey));

            if (showHidden)
                return items;

            var cacheKey = _cacheKeyService.PrepareKeyForDefaultCache(NopCatalogDefaults.ItemsDisplayedOnHomepageWithoutHiddenCacheKey,
                _storeContext.CurrentStore, _customerService.GetCustomerRoleIds(_workContext.CurrentCustomer));

            var result = _staticCacheManager.Get(cacheKey, () =>
            {
                return items
                    .Where(i => _aclService.Authorize(i) && _storeMappingService.Authorize(i))
                    .ToList();
            });

            return result;
        }

        public string GetFormattedBreadCrumb(DemoItem item, IList<DemoItem> allItems = null, string separator = ">>", int languageId = 0)
        {
            var result = string.Empty;

            var breadcrumb = GetItemBreadCrumb(item, allItems, true);
            for (var i = 0; i <= breadcrumb.Count - 1; i++)
            {
                var categoryName = _localizationService.GetLocalized(breadcrumb[i], x => x.Name, languageId);
                result = string.IsNullOrEmpty(result) ? categoryName : $"{result} {separator} {categoryName}";
            }

            return result;
        }

        public virtual IList<DemoItem> GetItemBreadCrumb(DemoItem item, IList<DemoItem> allitems = null, bool showHidden = false)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            var breadcrumbCacheKey = _cacheKeyService.PrepareKeyForDefaultCache(NopCatalogDefaults.ItemBreadcrumbCacheKey,
                item,
                _customerService.GetCustomerRoleIds(_workContext.CurrentCustomer),
                _storeContext.CurrentStore,
                _workContext.WorkingLanguage);

            return _staticCacheManager.Get(breadcrumbCacheKey, () =>
            {
                var result = new List<DemoItem>();

                //used to prevent circular references
                var alreadyProcessedItemIds = new List<int>();

                while (item != null && //not null
                       item.Status==1 && //Active
                       (showHidden || item.Published) && //published
                       (showHidden || _aclService.Authorize(item)) && //ACL
                       (showHidden || _storeMappingService.Authorize(item)) && //Store mapping
                       !alreadyProcessedItemIds.Contains(item.Id)) //prevent circular references
                {
                    result.Add(item);

                    alreadyProcessedItemIds.Add(item.Id);

                    item = allitems != null
                        ? allitems.FirstOrDefault(i => i.Id == item.Id)
                        : GetItemById(item.Id);
                }

                result.Reverse();

                return result;
            });
        }

        public DemoItem GetItemById(int itemId)
        {
            if (itemId == 0)
                return null;

            return _itemRepository.ToCachedGetById(itemId);
        }

        public List<DemoItem> GetItemByIds(int[] itemIds)
        {
            if (itemIds == null || itemIds.Length == 0)
                return new List<DemoItem>();

            var query = from i in _itemRepository.Table
                        where itemIds.Contains(i.Id) && i.Status==1
                        select i;

            return query.ToList();
        }

        public void InsertItem(DemoItem item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            _itemRepository.Insert(item);

            //event notification
            _eventPublisher.EntityInserted(item);
        }

        public void UpdateItem(DemoItem item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(DemoItem));

            _itemRepository.Update(item);

            _eventPublisher.EntityUpdated(item);
        }
    }
}
