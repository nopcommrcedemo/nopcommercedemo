﻿using System;
using System.Collections.Generic;
using System.Text;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Seo;
using Nop.Core.Domain.Stores;

namespace Nop.Core.Domain.Item
{
    public partial class DemoItem : BaseEntity, ILocalizedEntity, ISlugSupported, IAclSupported, IStoreMappingSupported
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public int SortOrder { get; set; }
        public bool Published { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime? UpdatedOnUtc { get; set; }
        public bool SubjectToAcl { get; set; }
        public bool LimitedToStores { get; set; }
        public bool ShowOnHomepage { get; set; }
        public string PageSizeOptions { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaTitle { get; set; }
        public string PriceRanges { get; set; }
        public int CategoryTemplateId { get; set; }
        public int PageSize { get; set; }
        public bool AllowCustomersToSelectPageSize { get; set; }
        public bool IncludeInTopMenu { get; set; }
    }
}
