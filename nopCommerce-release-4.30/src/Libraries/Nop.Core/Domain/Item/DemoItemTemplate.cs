﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Core.Domain.Item
{
    public partial class DemoItemTemplate : BaseEntity
    {
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the view path
        /// </summary>
        public string ViewPath { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public int DisplayOrder { get; set; }
    }
}
